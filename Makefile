PNG_DIR = cards-png
PDF_DIR = cards-pdf
CARD_TO_PDF = bin/card-to-pdf

CARDS = $(patsubst $(PNG_DIR)/%.png,%,$(wildcard $(PNG_DIR)/*.png))
PDF_CARDS = $(patsubst %,$(PDF_DIR)/%.pdf,$(CARDS))

all: pdf-cards

pdf-cards: $(PDF_CARDS)
$(PDF_DIR)/%.pdf: $(PNG_DIR)/%.png $(CARD_TO_PDF)
	$(CARD_TO_PDF) -d $(PDF_DIR) $<

clean:
	rm -rf $(PDF_DIR)
