This is **ThronesDB Print**, a suite of command lines tools to convert decks of
AGoT (A Game of Thrones, the card game) 2nd edition, recorded in various
formats, to compact printable PDF sheets.


# Installation

## Dependencies

- imagemagick          (for /usr/bin/{convert,identify})
- poppler-utils        (for /usr/bin/pdfunite)
- texlive-extra-utils  (for /usr/bin/pdfnup)

All dependencies are listed by their package name in the Debian distribution.


# Usage

## Initialization

Before using any of the tools below you should:

    source env.sh

It will set PYTHONPATH and PATH to suitable value to use the tools directly
from the Git repo.


## Retrieve or update raster card images from [ThronesDB][tdb]

    $ bin/download-card-images [--all|--new]

`--new` will try to download only PNG of cards that have not been downloaded in
the past; `--all` will (re-)download all of them.

Downloaded PNGs will be available under `./cards-png/`.

For this to work you need a local copy of [ThronesDB JSON data][tjd] is by
default assumed to reside in `./json`, which is a Git submodule pointing to the
repo.

[tbd]: https://thronesdb.com/
[tjd]: https://github.com/Alsciende/thronesdb-json-data


## Generate printable PDF card images

    $ make pdf-cards

Generated PDFs, one per card will be available under `./cards-pdf/`.


## Generate printable decks (from card codes)

A **textual deck** is a text file containing one card code (as
per [ThronesDB JSON data]([tjd])) per line. Multiple copies of the same card in
the deck should appear multiple times, one per line.

To generate compact, printable PDF sheets containing all deck cards:

    $ bin/deck-to-pdf mydeck.txt mydeck.pdf


## Generate printable decks (from OCTGN decks)

[OCTGN](http://octgn.net/) uses a XML-based deck format that has become a *de
facto* standard for many card games. Among others, ThronesDB allows to download
decks in OCTGN format. You can convert one such deck to printable PDF sheets as
follows:

    $ bin/deck-to-pdf mydeck.o8d mydeck.pdf


# Typography

## Physical cards

AGoT physical cards by Fantasy Flight Games are 88x62mm rectangles.

Some are printed landscape (plot cards), some portrait (most cards).

                                           /-------------\  ^
                                           |             |  |
    /---------------------\  ^             |             |  |
    |                     |  |             |             |  |
    |                     |                |    OTHER    |
    |      PLOT CARD      | 62 mm          |             | 88 mm
    |                     |                |    CARD     |
    |                     |  |             |             |  |
    \---------------------/  _             |             |  |
    <------- 88 mm ------->                |             |  |
                                           \-------------/  _
                                           <--- 62 mm --->

They have rounded corners, 2mm radius (XXX: this is a guess).


## Raster card images

Card images in PNG format are available from the ThronesDB website, but not
from the corresponding Git repositories. All retrieved images have the
following geometries (in pixel):

    $  cd ./cards-png/ ; identify *.png | cut -f 3 -d' ' | sort -u
    300x418
    300x419
    418x300
    419x300

The different orientations depend on whether the cards are portrait or
landscape. The off-by-1 pixel differences on the long side of cards seem to be
arbitrary.

A print resolution of 120 pixels per inch gives a print size that is about
63x88mm, i.e., 1mm larger on the short side than the correct print size.


# License

Copyright (C) Stefano Zacchiroli <zack@upsilon.cc>

This program is free software: you can redistribute it and/or modify it under
the terms of the GNU General Public License as published by the Free Software
Foundation, either version 3 of the License, or (at your option) any later
version.

A full copy of the GNU General Public License version 3 is available in the
top-level `LICENSE` file.
