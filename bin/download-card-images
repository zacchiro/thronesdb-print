#!/bin/bash

# retrieve or update a local cache of card images from <https://ThronesDB.com/>
#
# HTTP downloads rely on timestamping, so card images will be retrieved only if
# needed.
#
# HTTP downloads are sequential on purpose, to avoid hammering the remote
# server (and there aren't that many cards, so it doesn't take long to complete
# anyhow).

if [ -z "$CARDS_PNG_DIR" ] ; then
    echo "Undefined environment variable CARDS_PNG_DIR. Abort."
    echo "Missing 'source env.sh'?"
    exit 1
fi

CARD_IMAGE_TPL='https://thronesdb.com/bundles/cards/@CODE@.png'
CARD_IMAGE_DIR="$CARDS_PNG_DIR"
WGET_FLAGS='--timestamping --no-verbose'
WGET="wget $WGET_FLAGS"

ls_card_codes="ls-card-codes"

if [ -z "$1" -o \( "$1" != "--all" -a "$1" != "--new" \) ] ; then
    echo "Usage: download-card-images [--all|--new]"
    exit 0
fi

case "$1" in
    --all) download="all" ;;
    --new) download="new" ;;
esac

test -d "$CARD_IMAGE_DIR" || mkdir -p "$CARD_IMAGE_DIR"

"$ls_card_codes" |
    while read code ; do 
	card_url=${CARD_IMAGE_TPL/@CODE@/${code}}
	card_png="${code}.png"  # local to $CARD_IMAGE_DIR
	card_err="${code}.png.failed"

	if [ -f "${CARD_IMAGE_DIR}/${card_err}" ] ; then
	    continue  # skip cards that failed to download in the past
	fi
	if [ "$download" = "new" -a -f "${CARD_IMAGE_DIR}/${card_png}" ] ; then
	    continue  # skip download of already downloaded PNG
	fi

	echo "Retrieving image for card ${code}..."
	if ! (cd $CARD_IMAGE_DIR && $WGET "$card_url") ; then
	    touch "${CARD_IMAGE_DIR}/${card_err}"
	fi
    done
echo 'All done. Bye.'
