export PATH=$(pwd)/bin/:${PATH}
export PYTHONPATH=$(pwd)/python/:${PYTHONPATH}
export CARDS_JSON_DIR=$(pwd)/json
export CARDS_PNG_DIR=$(pwd)/cards-png
