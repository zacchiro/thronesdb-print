
import logging


def octgn_id_to_code(octgn_id, cards):
    matches = list(filter(lambda c: c.get('octgn_id') == octgn_id, cards))

    if not matches:
        return None
    if len(matches) > 1:
        logging.warn('multiple matches for OCTGN ID %s, picking one'
                     % octgn_id)

    return matches[0]['code']
