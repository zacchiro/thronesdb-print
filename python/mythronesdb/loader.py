
import json
import logging
import os.path

import xml.etree.ElementTree as ET

from mythronesdb import JSON_DATA_DIR


class LoadingError(RuntimeError):
    pass


def load_packs(datadir=JSON_DATA_DIR):
    """load a ThronesDB packs.json file and return an iterable over pack entry
    descriptions (JSON-based dictionaries)

    """
    json_f = os.path.join(datadir, 'packs.json')
    if not os.path.isfile(json_f):
        raise LoadingError('cannot find JSON pack index file: ' + json_f)

    packs = None
    with open(json_f) as f:
        packs = json.load(f)

    return packs


def load_factions(datadir=JSON_DATA_DIR):
    """load a ThronesDB factions.json file and return an iterable over faction
    descriptions (JSON-based dictionaries)

    """
    json_f = os.path.join(datadir, 'factions.json')
    if not os.path.isfile(json_f):
        raise LoadingError('cannot find JSON faction file: ' + json_f)

    factions = None
    with open(json_f) as f:
        factions = json.load(f)

    return factions


def iter_cards(packs=None, factions=None, datadir=JSON_DATA_DIR):
    """load all card from ThronesDB JSON data and return an iterable of card
    descriptions (JSON-based dictionaries)

    Load both card packs (from all different pack JSON files) and faction cards
    (from factions.json). As faction cards are not uniform in terms of
    available information with pack cards, add to them "type_code: faction" to
    allow spotting them.

    """
    if packs is None:
        packs = load_packs(datadir)
    if factions is None:
        factions = load_factions(datadir)

    for pack in packs:
        pack_code = pack['code']
        pack_json = os.path.join(datadir, 'pack', pack_code + '.json')
        if not os.path.isfile(pack_json):
            logging.warning('cannot file JSON pack file: ' + pack_json)
            continue

        with open(pack_json) as f:
            for card in json.load(f):
                yield card

    for faction in factions:
        yield faction


def load_cards(packs=None, factions=None, datadir=JSON_DATA_DIR):
    """like iter_cards, but return loaded cards as a list"""
    return list(iter_cards(packs, factions, datadir))


def octgn_iter_cards(xml_file):
    """load a OCTGN XML deck, and return an iterable over <card name, card UUID>
    pairs

    """
    xml_tree = ET.parse(xml_file)
    root = xml_tree.getroot()
    for card in root.iter('card'):
        (name, id, qty) = card.text, card.get('id'), int(card.get('qty'))
        for i in range(qty):
            yield (name, id)
